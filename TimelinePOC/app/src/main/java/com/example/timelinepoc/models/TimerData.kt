package com.example.timelinepoc.models

data class TimerData(
    val `data`: List<TimerData>,
    val time: Long,
    val title: String,
    val subtitle: String,
    val status: String
)