package com.example.timelinepoc.models

data class Data(
    val time: Int,
    val title: String,
    val status: String
)