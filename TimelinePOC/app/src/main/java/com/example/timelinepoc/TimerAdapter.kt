package com.example.timelinepoc

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.timelinepoc.models.TimerData
import java.text.SimpleDateFormat
import java.util.*

class TimerAdapter(private val layoutInflater: LayoutInflater,private var datalist: List<TimerData>) : RecyclerView.Adapter<TimerAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TimerAdapter.ViewHolder {
        val v = layoutInflater.inflate(R.layout.item_timer,
            parent,
            false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return datalist.size
    }

    override fun onBindViewHolder(holder: TimerAdapter.ViewHolder, position: Int) {
        val timer = datalist[position]
        holder.title.text = timer.title
        holder.subtitle.text = timer.time.toString()
        holder.time.text = getDateTime(timer.time)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val title: TextView = view.findViewById<View>(R.id.title) as TextView
        val subtitle: TextView = view.findViewById<View>(R.id.subtitle) as TextView
        val time: TextView = view.findViewById<View>(R.id.time) as TextView

    }

    fun resetAdapter(updatedlist: List<TimerData>){
        datalist = updatedlist
        notifyDataSetChanged()
    }

    fun getItem(position: Int): TimerData {
        return datalist[position]
    }

    private fun getDateTime(s: Long): String {
        try {
            var sdf = SimpleDateFormat("HH:mm")
            val netDate = Date(s * 1000)
            return sdf.format(netDate)
        } catch (e: Exception) {
            return e.toString()
        }
    }


}
