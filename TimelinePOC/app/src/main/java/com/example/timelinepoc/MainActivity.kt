package com.example.timelinepoc

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.timelinepoc.TimeLineDecoration.*
import com.example.timelinepoc.models.TimerData
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class MainActivity : AppCompatActivity() {

    var dataList:List<TimerData> = ArrayList()
    var isDayModeEnabled = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initView()
    }

    private fun initView() {
        loadData()
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.setHasFixedSize(true)
        recyclerView.isNestedScrollingEnabled = false
        recyclerView.itemAnimator = DefaultItemAnimator()
        val adapter = TimerAdapter(layoutInflater, dataList)
        val decoration: TimeLineDecoration = TimeLineDecoration(this)
            .setLineColor(R.color.light_grey)
            .setLineWidth(1F)
            .setLeftDistance(93)
            .setTopDistance(10)
            .setBeginMarker(R.drawable.begin_up)
            .setEndMarker(R.drawable.up)
            .setCustomMarker(R.drawable.up)
            .setMarkerRadius(4F)
            .setMarkerColor(R.color.colorAccent)
            .setCallback(object : TimeLineAdapter() {
                override fun getTimeLineType(position: Int): Int {
                    return if (position == 0) BEGIN else if (position == adapter.itemCount - 1) END else NORMAL
                }
            })
        recyclerView.addItemDecoration(decoration)
        recyclerView.adapter = adapter
    }

    private fun loadData() {
        try {
            val json_string = application.assets.open("timedata.json").bufferedReader().use {
                it.readText()
            }
            dataList = Gson().fromJson(json_string, Array<TimerData>::class.java).toList()
            println(dataList)
        }catch (e:Exception){
            println("Exception"+e.message)
        }
    }

    private fun isSection(time1:Long, time2:Long):Boolean{
        return Date(time1).date != Date(time2).date
    }
}